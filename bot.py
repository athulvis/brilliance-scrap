import time
import logging
import configparser
from telegram.ext import Updater, CommandHandler
from telegram import ParseMode
from Dbcreator import DBUpdater

class PscposterBot:
    def __init__(self,configfile):
        config = configparser.ConfigParser()
        config.read(configfile)
        self.token = config['KEYS']['TOKEN']

    def initbot(self):
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        updater = Updater(self.token, use_context=True)
        dp = updater.dispatcher
        dp.add_handler(CommandHandler("update", self.pscposter))
        updater.start_polling()
        updater.idle()

    def pscposter(self,update,context):
        dbu = DBUpdater('brildata.db')
        dat = dbu.db_select()
        for d in range(len(dat)):
            t_tit = dat[d][0].replace('&','&amp;').replace('(','\n\n').replace(')','\n\n').replace('-','\n\n')
            txt1 = f'\n\n<a href="{dat[d][1]}">{t_tit}</a>'+ '\n\n #questionpaper #previousqp\n\n @PadippuraQuestions\n\n'
            txt2 = f'\n\n<a href="{dat[d][2]}">{t_tit}</a>'+ '\n\n #answerkey #key\n\n @PadippuraQuestions\n\n'
            context.bot.send_message(chat_id = '@PadippuraQuestions', text = txt1, parse_mode='HTML')
            if dat[d][2]!='0':
                context.bot.send_message(chat_id = '', text = txt2, parse_mode='HTML')
            time.sleep(5)
            

if __name__ == '__main__':
    brPSC = PscposterBot('config')
    brPSC.initbot()
