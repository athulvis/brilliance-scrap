import sqlite3

class DBUpdater:
    """
    Object that abstrcts the database for storing the title and check for duplication later
    It uses sqlite3 as the database engine
    """

    def __init__(self, db_name):
        self.db_name = db_name

    def db_create(self):
        """
        Create new table if not exist in the database, create database if not found vacancy
        """
        try:
            # Creates or opens SQLite3 Database
            conn = sqlite3.connect(self.db_name, check_same_thread=False)
            c = conn.cursor()
            c.execute('''CREATE TABLE IF NOT EXISTS BRILDATA (id INT  PRIMARY KEY, title TEXT, qp TEXT, ans TEXT)''')
            status = True
        except sqlite3.Error as e:
            print(e.args[0])
            status= False
        finally:
            conn.close()
            return status

    def db_update(self,title,qp,ans):
        """
        Update the database with new data, call this method with parsed entry
        from the web. If the method encounters duplication in the data raises
        an exception and returns a False flag otherwise a True flag

        :param entry: string containing title parsed from the Web
        :return status: Boolean value indicating the result of database operation
        """
        try:
            # Creates or opens SQLite3 Database
            conn = sqlite3.connect(self.db_name, check_same_thread=False)
            c = conn.cursor()
            for i in range(len(title)):
                 conn.execute("INSERT INTO BRILDATA VALUES (?,?,?,?)",(i,title[i],qp[i],ans[i]));
            conn.commit()
            status= True
        except sqlite3.IntegrityError as e:
            print(e.args[0])
            status= False
        finally:
            conn.close()
            return status

    def db_select(self):
        conn = sqlite3.connect(self.db_name, check_same_thread=False)
        c = conn.cursor()
        cursor = conn.execute("SELECT title, qp, ans from BRILDATA")
        dat = cursor.fetchall() 
        conn.close()
        return dat


