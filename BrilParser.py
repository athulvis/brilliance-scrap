from bs4 import BeautifulSoup
import requests as req
import numpy as np
import itertools
from Dbcreator import DBUpdater

class BrillianceParser:
    
    def __init__(self):
        pass
    
    def count_updater(self,y,n):
        br_title=[];br_qp=[]
        for i in range(1,n+1):  
            try:
                url = f"https://brilliancecollege.com/kerala_psc_ssc_bank_previous_question_paper.php?year={y}&PageNo={i}"
                page = req.get(url)
                soup = BeautifulSoup(page.text,'lxml')
                btit = soup.find_all('span', attrs={'class':'searchRestitle'})
                bqp = soup.find_all('a', attrs={'class':'files'})
            except:
                pass
            if btit != [] and bqp !=[]:
                br_title.append(btit)
                br_qp.append(bqp)
        return br_title,br_qp
    
    def br_parser(self):
        ti_link=[];qp_link=[]
        yr_list2 = list(range(2003,2021))
        yr_list = ['Previous Years'] + yr_list2
        for y in yr_list:
            if y == 'Previous Years':
                n=10
            else:
                n=3
            d1, d2 = self.count_updater(y,n)
            ti_link.append(d1)
            qp_link.append(d2)
        return [ti_link,qp_link]
    
    def generator(self):
        global brtit
        tpar=[];qp_par=[];anspar=[]
        dt,dqp = self.br_parser()
        dt1 = list(itertools.chain.from_iterable(dt))
        d1 = list(itertools.chain.from_iterable(dt1))
        dqp1 = list(itertools.chain.from_iterable(dqp))
        d2 = list(itertools.chain.from_iterable(dqp1))
        for i in range(len(d1)):
            title = d1[i].text
            qp = "https://brilliancecollege.com/"+d2[i].get('href').replace(' ','%20')
            ans_txt = d2[i].find_next('a').text
            if ans_txt !='Answer':
                ans = 0
            else:
                ans = "https://brilliancecollege.com/"+d2[i].find_next('a').get('href').replace(' ','%20')
            tpar.append(title)
            qp_par.append(qp)
            anspar.append(ans)
        return tpar,qp_par,anspar


