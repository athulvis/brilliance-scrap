from Dbcreator import DBUpdater
from BrilParser import BrillianceParser


title, qp,ans = BrillianceParser().generator()
dbu = DBUpdater('brildata.db')
dbu.db_create()
dbu.db_update(title,qp,ans)
